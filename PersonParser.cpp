#include <iostream>

#include "PersonParser.h"
#include <iostream>

using namespace std;

void PersonParser::parse(std::string fullname) {
     surname = fullname.substr(fullname.rfind(" ") + 1,fullname.size());
     name = fullname.substr(0,fullname.rfind(" "));
}

const std::string &PersonParser::getName() const {
    return name;
}

const std::string &PersonParser::getSurname() const {
    return surname;
}

PersonParser::PersonParser(std::string fullname) {
    if(fullname.rfind(" ") == fullname.npos){
        throw(0);
    }
    parse(fullname);
}
