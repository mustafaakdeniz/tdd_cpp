#include <gtest/gtest.h>
#include "../PersonParser.h"
#include "PersonParser_Test.h"

using namespace std;

TEST(it_should_parse_from_space, PersonTest1) {
    string testStr = "Mahmut Tuncer";
    PersonParser p(testStr);
    EXPECT_EQ("Mahmut" , p.getName());
    EXPECT_EQ("Tuncer", p.getSurname());
}

TEST(it_should_parse_from_space_with_multiple_names, PersonTest2){
    string testStr = "Eyup Sabri Tuncer";
    PersonParser p(testStr);
    EXPECT_EQ("Eyup Sabri" , p.getName());
    EXPECT_EQ("Tuncer", p.getSurname());
}

TEST(it_should_throw_exception_if_name_not_contains_space, PersonTest3) {
    string testStr = "Mahmut";
    EXPECT_ANY_THROW(PersonParser p(testStr));
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}