#if !defined(PERSON_PARSER_H)
#define PERSON_PARSER_H

#include <string>

class PersonParser{
private:
    std::string name,surname;
public:
    PersonParser(std::string fullname);
    void parse (std::string fullname);

    const std::string &getName() const;
    const std::string &getSurname() const;
};

#endif // PERSON_PARSER_H
